"use strict"

function createNewUser(){
    const name = prompt ("Enter your name");
    const surname = prompt ("Enter your surname");

    const newUser = {
        _firstName: name,
        _lastName: surname,

        getLogin: function(){
            console.log(`${this._firstName[0]}${this._lastName}`.toLowerCase());
        }

        ,
        setFirstName: function(name){
            this._firstName = name;
        }

        ,
        setLastName: function(surname){
            this._lastName = surname;
        }
    };
    
    return newUser;
}

const newUser = createNewUser();
newUser.getLogin();